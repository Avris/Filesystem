import {inspect} from 'util';
import * as jssha from 'jssha';

export function trim(str: string, chr: string = '\\s'): string {
    return str.replace(new RegExp('^' + chr + '+|' + chr + '+$', 'g'), '');
}

export function ltrim(str: string, chr: string = '\\s'): string {
    return str.replace(new RegExp('^' + chr + '+'), '');
}

export function rtrim(str: string, chr: string = '\\s'): string {
    return str.replace(new RegExp(chr + '+$'), '');
}

export function substring(str: string, start: number, end?: number): string {
    if (start < 0) {
        start = str.length + start;
    }

    if (end && end < 0) {
        end = str.length + end;
    }

    return str.substring(start, end);
}

export function startsWith(haystack: string, needle: string): boolean {
    return substring(haystack, 0, needle.length) === needle;
}

export function endsWith(haystack: string, needle: string): boolean {
    return substring(haystack, haystack.length - needle.length) === needle;
}

export function contains(haystack: string, needle: string): boolean {
    return haystack.indexOf(needle) >= 0;
}

export function dump(...objects) {
    console.log(...objects.map((obj) => inspect(obj, { colors: true, depth: Infinity })));
}

export function hmacSha1(message: string, key: string) {
    const shaObj = new jssha('SHA-1', 'TEXT');
    shaObj.setHMACKey(key, 'TEXT');
    shaObj.update(message);
    return shaObj.getHMAC('B64');
}

export function queryString(parameters): string {
    const encoded = [];

    for(let key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            encoded.push(encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]));
        }
    }

    return encoded.join('&');
}

export function arrayify(obj: any) {
    if (obj === undefined) {
        return [];
    }

    if (Array.isArray(obj)) {
        return obj;
    }

    return [obj];
}

export function getHeaders(res) {
    const headers = {};

    res.headers.forEach(function(value, name) {
        headers[name] = value;
    });

    return headers;
}

export function toArrayBuffer(buffer: Buffer): ArrayBuffer {
    const ab = new ArrayBuffer(buffer.length);
    const view = new Uint8Array(ab);
    for (let i = 0; i < buffer.length; ++i) {
        view[i] = buffer[i];
    }

    return ab;
}

export function toBuffer(ab: ArrayBuffer): Buffer {
    const buf = Buffer.alloc(ab.byteLength);
    const view = new Uint8Array(ab);
    for (let i = 0; i < buf.length; ++i) {
        buf[i] = view[i];
    }

    return buf;
}
