export enum Visibility {
    Private = 'private',
    Public = 'public',
}

export class Metadata {
    path: string;
    isDir: boolean;
    timestamp: number;
    size: number;
    visibility: Visibility;
}

export class BufferFile extends Metadata {
    content: Buffer | null;
}

export class StringFile extends Metadata {
    content: string | null;
}

export interface AdapterInterface {
    list(directory: string, recursive: boolean): Promise<{[path: string]: Metadata}>;
    has(path: string): Promise<boolean>;
    getMetadata(path: string): Promise<Metadata>;
    read(path: string): Promise<Buffer>;
    create(path: string, content: Buffer): Promise<Metadata>;
    update(path: string, content: Buffer): Promise<Metadata>;
    move(path: string, newPath: string): Promise<Metadata>;
    copy(path: string, newPath: string): Promise<Metadata>;
    remove(path: string): Promise<void>;
    publish(path: string): Promise<Metadata>;
    unpublish(path: string): Promise<Metadata>;
    createDir(path: string): Promise<Metadata>;
    removeDir(path: string): Promise<void>;
}

export interface FilesystemInterface {
    list(directory: string, recursive: boolean): Promise<{ [path: string]: Metadata }>;
    has(path: string): Promise<boolean>;
    getMetadata(path: string): Promise<Metadata>;
    read(path: string): Promise<Buffer>;
    create(path: string, content: Buffer): Promise<Metadata>;
    update(path: string, content: Buffer): Promise<Metadata>;
    put(path: string, content: Buffer): Promise<Metadata>;
    move(path: string, newPath: string): Promise<Metadata>;
    copy(path: string, newPath: string): Promise<Metadata>;
    remove(path: string): Promise<void>;
    publish(path: string): Promise<Metadata>;
    unpublish(path: string): Promise<Metadata>;
    createDir(path: string): Promise<Metadata>;
    removeDir(path: string): Promise<void>;
}
