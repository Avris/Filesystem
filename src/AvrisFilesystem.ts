import {AdapterInterface, FilesystemInterface, Metadata} from "./types";

export class AvrisFilesystem implements FilesystemInterface{
    constructor (private adapter: AdapterInterface) {

    }

    async list(directory: string = '', recursive: boolean = false): Promise<{[path: string]: Metadata}> {
        return this.adapter.list(AvrisFilesystem.normalisePath(directory), recursive);
    }

    async has(path: string): Promise<boolean> {
        return await this.adapter.has(AvrisFilesystem.normalisePath(path));
    }

    async getMetadata(path: string): Promise<Metadata> {
        return await this.adapter.getMetadata(
            await this.assertPresent(AvrisFilesystem.normalisePath(path))
        );
    }

    async read(path: string): Promise<Buffer> {
        return await this.adapter.read(
            await this.assertPresent(AvrisFilesystem.normalisePath(path))
        );
    }

    async create(path: string, content: Buffer): Promise<Metadata> {
        return await this.adapter.create(
            await this.assertAbsent(AvrisFilesystem.normalisePath(path)),
            content
        );
    }

    async update(path: string, content: Buffer): Promise<Metadata> {
        return await this.adapter.update(
            await this.assertPresent(AvrisFilesystem.normalisePath(path)),
            content
        );
    }

    async put(path: string, content: Buffer): Promise<Metadata> {
        path = AvrisFilesystem.normalisePath(path);

        if (await this.adapter.has(path)) {
            return await this.adapter.update(path, content);
        }

        return await this.adapter.create(path, content);
    }

    async move(path: string, newPath: string): Promise<Metadata> {
        return await this.adapter.move(
            await this.assertPresent(AvrisFilesystem.normalisePath(path)),
            await this.assertAbsent(AvrisFilesystem.normalisePath(newPath))
        );
    }

    async copy(path: string, newPath: string): Promise<Metadata> {
        return await this.adapter.copy(
            await this.assertPresent(AvrisFilesystem.normalisePath(path)),
            await this.assertAbsent(AvrisFilesystem.normalisePath(newPath))
        );
    }

    async remove(path: string): Promise<void> {
        await this.adapter.remove(
            await this.assertPresent(AvrisFilesystem.normalisePath(path))
        );
    }

    async publish(path: string): Promise<Metadata> {
        return await this.adapter.publish(
            await this.assertPresent(AvrisFilesystem.normalisePath(path))
        );
    }

    async unpublish(path: string): Promise<Metadata> {
        return await this.adapter.unpublish(
            await this.assertPresent(AvrisFilesystem.normalisePath(path))
        );
    }

    async createDir(path: string): Promise<Metadata> {
        return await this.adapter.createDir(
            await this.assertAbsent(AvrisFilesystem.normalisePath(path))
        );
    }

    async removeDir(path: string): Promise<void> {
        await this.adapter.removeDir(
            await this.assertPresent(AvrisFilesystem.normalisePath(path))
        );
    }

    private async assertPresent(path: string) {
        if (!await this.adapter.has(path)) {
            throw `File "${path}" does not exist`;
        }

        return path;
    }

    private async assertAbsent(path: string) {
        if (await this.adapter.has(path)) {
            throw `File "${path}" already exists`;
        }

        return path;
    }

    private static normalisePath(path: string): string
    {
        const parts = [];

        for (let part of path.replace('\\', '/').split('/')) {
            switch (part) {
                case '':
                case '.':
                    break;
                case '..':
                    if (parts.length === 0) {
                        throw `Path "${path}" outside of the root`
                    }
                    parts.pop();
                    break;
                default:
                    parts.push(part);
            }
        }

        return parts.join('/');
    }
}
