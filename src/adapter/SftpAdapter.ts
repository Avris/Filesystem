import {AdapterInterface, Metadata, Visibility} from "../types";
import * as fs from 'fs';
import * as FTPClient from 'ssh2-sftp-client';
import {rtrim} from "../helpers";

export class SftpAdapter implements AdapterInterface {
    private client: FTPClient = null;

    static Permissions = {
        File: {
            Public: 0o644,
            Private: 0o600,
        },
        Dir: {
            Public: 0o755,
            Private: 0o700,
        }
    };

    constructor (private host, private user, private key, private dir) {
        this.dir = rtrim(dir.replace('\\', '/'), '/') + '/'

        // TODO this.client.end();
    }

    async list(directory: string, recursive: boolean): Promise<{ [path: string]: Metadata }> {
        await this.connect();

        const objects = {};

        const fetchList = async (dir: string) => {
            await this.client.list(this.dir + dir).then(await (async (res) => {
                for (let obj of res) {
                    const path = dir + '/' + obj.name;
                    if (obj.type === 'd') {
                        objects[path] = {
                            path: path,
                            isDir: true,
                            timestamp: Math.round(obj.modifyTime / 1000),
                            size: obj.size,
                            visibility: SftpAdapter.getVisibility(obj),
                        };

                        if (recursive) {
                            await fetchList(dir + '/' + obj.name);
                        }
                    } else {
                        objects[path] = {
                            path: path,
                            isDir: false,
                            timestamp: Math.round(obj.modifyTime / 1000),
                            size: obj.size,
                            visibility: SftpAdapter.getVisibility(obj),
                        }
                    }
                }
            }));
        };

        await fetchList(directory);

        return objects;
    }

    async has(path: string): Promise<boolean> {
        await this.connect();
        const [dir, name] = SftpAdapter.splitIntoDirName(path);

        return await this.client.list(this.dir + dir).then(await (async (res) => {
            for (let obj of res) {
                if (obj.name === name) {
                    return true;
                }
            }

            return false;
        })).catch((err) => {
            return false;
        });
    }

    async getMetadata(path: string): Promise<Metadata> {
        await this.connect();
        const [dir, name] = SftpAdapter.splitIntoDirName(path);

        return await this.client.list(this.dir + dir).then(await (async (res) => {
            for (let obj of res) {
                if (obj.name === name) {
                    return {
                        path: path,
                        isDir: obj.type === 'd',
                        timestamp: Math.round(obj.modifyTime / 1000),
                        size: obj.size,
                        visibility: SftpAdapter.getVisibility(obj),
                    };
                }
            }
        }));
    }

    async read(path: string): Promise<Buffer> {
        await this.connect();

        return (await this.client.get(this.dir + path, true, null));
    }

    async create(path: string, content: Buffer): Promise<Metadata> {
        await this.connect();
        await this.ensureDirectory(path);

        await this.client.put(content, this.dir + path);

        return await this.getMetadata(path);
    }

    async update(path: string, content: Buffer): Promise<Metadata> {
        await this.connect();
        await this.ensureDirectory(path);

        await this.client.put(content, this.dir + path);

        return await this.getMetadata(path);
    }

    async move(path: string, newPath: string): Promise<Metadata> {
        await this.connect();
        await this.ensureDirectory(newPath);

        await this.client.rename(this.dir + path, this.dir + newPath);

        return await this.getMetadata(newPath);
    }

    async copy(path: string, newPath: string): Promise<Metadata> {
        await this.connect();
        await this.ensureDirectory(newPath);

        await this.client.get(this.dir + path, true, null).then(await (async (stream) => {
            await this.client.put(stream, this.dir + newPath, true, null);
        }));

        return await this.getMetadata(newPath);
    }

    async remove(path: string): Promise<void> {
        await this.connect();

        await this.client.delete(this.dir + path);
    }

    async publish(path: string): Promise<Metadata> {
        await this.connect();

        const meta = await this.getMetadata(path);

        await this.client.chmod(
            this.dir + path,
            meta.isDir
                ? SftpAdapter.Permissions.Dir.Public
                : SftpAdapter.Permissions.File.Public
        );

        meta.visibility = Visibility.Public;

        return meta;
    }

    async unpublish(path: string): Promise<Metadata> {
        await this.connect();

        const meta = await this.getMetadata(path);

        await this.client.chmod(
            this.dir + path,
            meta.isDir
                ? SftpAdapter.Permissions.Dir.Private
                : SftpAdapter.Permissions.File.Private
        );

        meta.visibility = Visibility.Private;

        return meta;
    }

    async createDir(path: string): Promise<Metadata> {
        await this.connect();

        await this.client.mkdir(this.dir + path, true);

        return await this.getMetadata(path);
    }

    async removeDir(path: string): Promise<void> {
        await this.connect();

        await this.client.rmdir(this.dir + path, true);
    }

    private async connect() {
        if (this.client === null) {
            this.client = new FTPClient();

            await this.client.connect({
                host: this.host,
                username: this.user,
                privateKey: fs.readFileSync(this.key),
            });
        }

        return this.client;
    }

    private static getVisibility(obj): Visibility {
        return obj.rights.other.indexOf('r') > -1
            ? Visibility.Public
            : Visibility.Private;
    }

    private static splitIntoDirName(path: string): [string, string] {
        const parts = path.split('/');
        const name = parts.pop();
        const dir = parts.join('/');

        return [dir, name];
    }

    private async ensureDirectory(path: string) {
        const [dir, name] = SftpAdapter.splitIntoDirName(path);

        if (!await this.has(dir)) {
            await this.client.mkdir(this.dir + dir, true);
        }
    }
}
