import {AdapterInterface, Metadata, Visibility} from "../types";

export class NullAdapter implements AdapterInterface {
    async list(directory: string, recursive: boolean): Promise<{ [path: string]: Metadata }> {
        return {};
    }

    async has(path: string): Promise<boolean> {
        return true;
    }

    async getMetadata(path: string): Promise<Metadata> {
        return {
            path: path,
            isDir: false,
            timestamp: Math.round(+Date.now() / 1000),
            size: 0,
            visibility: Visibility.Private,
        };
    }

    async read(path: string): Promise<Buffer> {
        return Buffer.from('');
    }

    async create(path: string, content: Buffer): Promise<Metadata> {
        return await this.getMetadata(path);
    }

    async update(path: string, content: Buffer): Promise<Metadata> {
        return await this.getMetadata(path);
    }

    async move(path: string, newPath: string): Promise<Metadata> {
        return await this.getMetadata(newPath);
    }

    async copy(path: string, newPath: string): Promise<Metadata> {
        return await this.getMetadata(newPath);
    }

    async remove(path: string): Promise<void> {
    }

    async publish(path: string): Promise<Metadata> {
        return this.getMetadata(path);
    }

    async unpublish(path: string): Promise<Metadata> {
        return this.getMetadata(path);
    }

    async createDir(path: string): Promise<Metadata> {
        return this.getMetadata(path);
    }

    async removeDir(path: string): Promise<void> {
    }
}
