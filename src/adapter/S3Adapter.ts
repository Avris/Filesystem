import {Metadata, Visibility, AdapterInterface} from '../types';
import * as fetch from 'node-fetch';
import {xml2js} from 'xml-js';
import {arrayify, endsWith, getHeaders, hmacSha1, ltrim, queryString, rtrim, startsWith, toBuffer} from "../helpers";

export class S3Adapter implements AdapterInterface {
    private PUBLIC_GRANT_URL: string = 'http://acs.amazonaws.com/groups/global/AllUsers';

    constructor(private bucket, private key, private secret) {
    }

    async list(directory: string, recursive: boolean): Promise<{ [path: string]: Metadata }> {
        const params = {
            'list-type': '2',
            'prefix': ltrim(rtrim(directory, '/') + '/', '/'),
            'delimiter': recursive ? '' : '/',
        };

        return await this.request('?' + queryString(params)).then((res) => {
            return res.text();
        }).then((txt) => {
            const data = xml2js(txt, {compact: true})['ListBucketResult'];
            // TODO data.IsTruncated pagination

            const objects = {};

            for (let obj of arrayify(data.Contents)) {
                const path = obj.Key._text;
                const isDir = endsWith(path, '/');
                const ts = +new Date(obj.LastModified._text) / 1000;

                objects[path] = {
                    path: path,
                    isDir: isDir,
                    timestamp: ts,
                    size: parseInt(obj.Size._text),
                    visibility: null,
                };

                if (!isDir) {
                    const parts = path.split('/');
                    for (let i = 1; i < parts.length; i++) {
                        const key = parts.slice(0, i).join('/') + '/';
                        if (objects[key] === undefined) {
                            objects[key] = {
                                path: key,
                                isDir: true,
                                timestamp: 0,
                                size: 0,
                                visibility: null,
                            }
                        }
                    }
                }
            }

            for (let obj of arrayify(data.CommonPrefixes)) {
                const path = obj.Prefix._text;
                objects[path] = {
                    path: path,
                    isDir: true,
                    timestamp: 0,
                    size: 0,
                    visibility: null,
                }
            }

            return objects;
        });
    }

    async has(path: string): Promise<boolean> {
        return await this.doesFileExist(path) || await this.doesDirectoryExist(path);
    }

    async getMetadata(path: string): Promise<Metadata> {
        return await this.request(encodeURIComponent(path), {method: 'HEAD'}).then(await (async (res) => {
            if (res.status === 404 && await this.doesDirectoryExist(path)) {
                return {
                    path: path,
                    isDir: true,
                    timestamp: 0,
                    size: 0,
                    visibility: Visibility.Private,
                };
            }

            const headers = getHeaders(res);

            return {
                path: path,
                isDir: false,
                timestamp: Math.round(+new Date(headers['last-modified']) / 1000),
                size: parseInt(headers['content-length']),
                visibility: await this.getVisibility(path),
            };
        }));
    }

    async read(path: string): Promise<Buffer> {
        return await this.request(encodeURIComponent(path)).then((res) => {
            return res.arrayBuffer();
        }).then((ab) => {
            return toBuffer(ab);
        });
    }

    async create(path: string, content: Buffer): Promise<Metadata> {
        await this.upload(path, content);

        return await this.getMetadata(path);
    }

    async update(path: string, content: Buffer): Promise<Metadata> {
        await this.upload(path, content);

        return await this.getMetadata(path);
    }

    async move(path: string, newPath: string): Promise<Metadata> {
        const meta: Metadata = await this.copy(path, newPath);

        await this.remove(path);

        return meta;
    }

    async copy(path: string, newPath: string): Promise<Metadata> {
        await this.request(encodeURIComponent(newPath), {
            method: 'PUT',
            headers: {
                'x-amz-copy-source': `/${this.bucket}/${encodeURIComponent(path)}`,
            }
        });

        return await this.getMetadata(newPath);
    }

    async remove(path: string): Promise<void> {
        await this.request(encodeURIComponent(path), {method: 'DELETE'});
    }

    async publish(path: string): Promise<Metadata> {
        await this.request(encodeURIComponent(path), {
            method: 'PUT',
            action: 'acl',
            headers: {
                'x-amz-acl': 'public-read',
            }
        });

        return this.getMetadata(path);
    }

    async unpublish(path: string): Promise<Metadata> {
        await this.request(encodeURIComponent(path), {
            method: 'PUT',
            action: 'acl',
            headers: {
                'x-amz-acl': 'private',
            }
        });

        return this.getMetadata(path);
    }

    async createDir(path: string): Promise<Metadata> {
        await this.upload(path + '/', Buffer.from(''));

        return this.getMetadata(path);
    }

    async removeDir(path: string): Promise<void> {
        const files = await this.list(path, true);

        const removePromises = [];

        for (let file in files) {
            if (files.hasOwnProperty(file) && !files[file].isDir) {
                removePromises.push(this.remove(file));
            }
        }

        if (removePromises.length) {
            await Promise.all(removePromises);
        }
    }

    private async request(uri: string, options: any = {}) {
        const date = new Date().toUTCString();

        if (options.headers === undefined) {
            options.headers = {};
        }

        options.headers['date'] = date;
        options.headers['authorization'] = this.buildAuthHeader(uri, options);

        if (options.action !== undefined) {
            uri += '?' + options.action;
            delete options.action;
        }

        return fetch(`https://${this.bucket}.s3.amazonaws.com/${uri}`, options)
    }

    private buildAuthHeader(uri: string, options: any): string {
        let stringToSign = '';

        stringToSign += options.method || 'GET';
        stringToSign += "\n";

        // TODO content
        stringToSign += "\n";

        stringToSign += options.headers['content-type'] || '';
        stringToSign += "\n";

        stringToSign += options.headers['date'];
        stringToSign += "\n";

        for (let key in options.headers) {
            if (options.headers.hasOwnProperty(key) && startsWith(key, 'x-amz-')) {
                stringToSign += key + ':' + options.headers[key];
                stringToSign += "\n";
            }
        }

        const qmPos = uri.indexOf('?');
        if (qmPos >= 0) {
            uri = uri.slice(0, qmPos);
        }
        if (options.action !== undefined) {
            uri += '?' + options.action;
        }
        stringToSign += `/${this.bucket}/${uri}`;

        const signature = hmacSha1(stringToSign, this.secret);

        return `AWS ${this.key}:${signature}`;
    }

    private async doesFileExist(path: string): Promise<boolean> {
        return await this.request(encodeURIComponent(path), {method: 'HEAD'}).then((res) => {
            if (res.status === 404) {
                return false;
            }

            if (res.status === 200) {
                return true;
            }

            // TODO handle access denied, >=500
        });
    }

    private async doesDirectoryExist(path: string): Promise<boolean> {
        const params = {
            'list-type': '2',
            'prefix': ltrim(rtrim(path, '/') + '/', '/'),
            'max-keys': '1',
        };

        return await this.request('?' + queryString(params)).then((res) => {
            return res.text();
        }).then((txt) => {
            const data = xml2js(txt, {compact: true})['ListBucketResult'];

            return arrayify(data.Contents).length + arrayify(data.CommonPrefixes).length > 0;
        });
    }

    private async getVisibility(path: string): Promise<Visibility> {
        return await this.request(encodeURIComponent(path), {action: 'acl'}).then((res) => {
            return res.text();
        }).then((txt) => {
            const data = xml2js(txt, {compact: true})['AccessControlPolicy'];

            for (let grant of arrayify(data['AccessControlList']['Grant'])) {
                if (grant['Grantee']['URI']
                    && grant['Grantee']['URI']._text === this.PUBLIC_GRANT_URL
                    && grant['Permission']._text === 'READ'
                ) {
                    return Visibility.Public;
                }
            }

            return Visibility.Private;
        });
    }

    private async upload(path: string, content: Buffer) {
        return this.request(encodeURIComponent(path), {
            method: 'PUT',
            body: content,
            headers: {
                'content-type': 'text/plaintext', // TODO
                'content-length': content.length,
            }
        });
    }
}
