import {AdapterInterface, Metadata, StringFile, Visibility} from "../types";
import {contains, startsWith, substring} from "../helpers";

export class BrowserStorageAdapter implements AdapterInterface {
    private storage: Storage;

    constructor(storage: Storage | null, private prefix: string = 'afs-') {
        this.storage = storage || window.localStorage;
    }

    async list(directory: string, recursive: boolean): Promise<{ [path: string]: Metadata }> {
        const objects = {};

        for (let path in this.storage) {
            if (!startsWith(path, this.prefix)) {
                continue;
            }

            path = path.substring(this.prefix.length);

            if (path === directory) {
                continue;
            }

            if (!startsWith(path, directory)) {
                continue;
            }

            if (!recursive && contains(substring(path, directory.length + 1), '/')) {
                continue;
            }

            objects[path] = <Metadata> JSON.parse(this.storage.getItem(this.prefix + path));
        }

        return objects;
    }

    async has(path: string): Promise<boolean> {
        return this.storage.getItem(this.prefix + path) !== null;
    }

    async getMetadata(path: string): Promise<Metadata> {
        return JSON.parse(this.storage.getItem(this.prefix + path));
    }

    async read(path: string): Promise<Buffer> {
        return Buffer.from(JSON.parse(this.storage.getItem(this.prefix + path)).content, 'base64');
    }

    async create(path: string, content: Buffer): Promise<Metadata> {
        this.ensureDirectory(BrowserStorageAdapter.dirname(path));

        const file: StringFile = {
            path: path,
            isDir: false,
            timestamp: Math.round(+Date.now() / 1000),
            size: content.length,
            visibility: Visibility.Private,
            content: content.toString('base64'),
        };

        this.storage.setItem(this.prefix + path, JSON.stringify(file));

        return file;
    }

    async update(path: string, content: Buffer): Promise<Metadata> {
        const file = JSON.parse(this.storage.getItem(this.prefix + path));

        file.content = content.toString('base64');
        file.size = content.length;

        this.storage.setItem(this.prefix + path, JSON.stringify(file));

        return file;
    }

    async move(path: string, newPath: string): Promise<Metadata> {
        this.ensureDirectory(BrowserStorageAdapter.dirname(newPath));

        const file = JSON.parse(this.storage.getItem(this.prefix + path));
        file.path = newPath;
        this.storage.setItem(this.prefix + newPath, JSON.stringify(file));
        this.storage.removeItem(this.prefix + path);

        return file;
    }

    async copy(path: string, newPath: string): Promise<Metadata> {
        this.ensureDirectory(BrowserStorageAdapter.dirname(newPath));

        const file = JSON.parse(this.storage.getItem(this.prefix + path));
        const newFile = <StringFile> {...file, path: newPath};

        this.storage.setItem(this.prefix + newPath, JSON.stringify(file));

        return newFile;
    }

    async remove(path: string): Promise<void> {
        this.storage.removeItem(this.prefix + path);
    }

    async publish(path: string): Promise<Metadata> {
        const file = JSON.parse(this.storage.getItem(this.prefix + path));

        file.visibility = Visibility.Public;

        this.storage.setItem(this.prefix + path, JSON.stringify(file));

        return file;
    }

    async unpublish(path: string): Promise<Metadata> {
        const file = JSON.parse(this.storage.getItem(this.prefix + path));

        file.visibility = Visibility.Private;

        this.storage.setItem(this.prefix + path, JSON.stringify(file));

        return file;
    }

    async createDir(path: string): Promise<Metadata> {
        this.ensureDirectory(path);

        return await this.getMetadata(path);
    }

    async removeDir(path: string): Promise<void> {
        for (let file in this.storage) {
            if (startsWith(file, this.prefix + path)) {
                this.storage.removeItem(this.prefix + file);
            }
        }
    }

    private static dirname(path: string) {
        const parts = path.split('/');
        parts.pop();

        return parts.join('/');
    }

    private ensureDirectory(path: string) {
        const parts = path.split('/');
        const done = [];

        for (let part of parts) {
            done.push(part);

            this.doEnsureDirectory(done.join('/'))
        }
    }

    private doEnsureDirectory(path: string) {
        if (this.storage.getItem(this.prefix + path) !== null) {
            if (!JSON.parse(this.storage.getItem(this.prefix + path)).isDir) {
                throw `Cannot create directory "${path}", it's already a file`;
            }

            return;
        }

        const file = <StringFile> {
            path: path,
            isDir: true,
            timestamp: Math.round(+Date.now() / 1000),
            size: 0,
            visibility: Visibility.Private,
            content: null,
        };

        this.storage.setItem(this.prefix + path, JSON.stringify(file));
    }
}
