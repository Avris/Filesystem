import {Metadata, Visibility, AdapterInterface} from "../types";
import * as fs from 'fs';
import {rtrim, substring, trim} from "../helpers";

export class LocalAdapter implements AdapterInterface {
    private dir: string;

    static Permissions = {
        File: {
            Public: 0o644,
            Private: 0o600,
        },
        Dir: {
            Public: 0o755,
            Private: 0o700,
        }
    };

    constructor(dir: string) {
        this.dir = rtrim(dir.replace('\\', '/'), '/') + '/';

        if (!fs.existsSync(this.dir) || !fs.lstatSync(this.dir).isDirectory()) {
            throw `Cannot use "${this.dir}" as a root. Make sure it exists and is a directory.`;
        }
    }

    async list(directory: string, recursive: boolean): Promise<{ [path: string]: Metadata }> {
        const objects = {};

        for (let file of fs.readdirSync(this.dir + directory)) {
            const path = trim(directory + '/' + file, '/');
            const object = this.readMetadata(path);

            objects[path] = object;

            if (recursive && object.isDir) {
                const subObjects = await this.list(path, recursive);

                for (let subObject in subObjects) {
                    objects[subObject] = subObjects[subObject];
                }
            }
        }

        return objects;
    }

    async has(path: string): Promise<boolean> {
        return fs.existsSync(this.dir + path);
    }

    async getMetadata(path: string): Promise<Metadata> {
        return this.readMetadata(path);
    }

    async read(path: string): Promise<Buffer> {
        return fs.readFileSync(this.dir + path);
    }

    async create(path: string, content: Buffer): Promise<Metadata> {
        this.ensureDirectory(LocalAdapter.dirname(path));

        fs.writeFileSync(this.dir + path, content, {mode: LocalAdapter.Permissions.File.Private});

        return this.readMetadata(path);
    }

    async update(path: string, content: Buffer): Promise<Metadata> {
        fs.writeFileSync(this.dir + path, content);

        return this.readMetadata(path);
    }

    async move(path: string, newPath: string): Promise<Metadata> {
        this.ensureDirectory(LocalAdapter.dirname(newPath));

        fs.renameSync(this.dir + path, this.dir + newPath);

        return this.readMetadata(newPath);
    }

    async copy(path: string, newPath: string): Promise<Metadata> {
        this.ensureDirectory(LocalAdapter.dirname(newPath));

        fs.copyFileSync(this.dir + path, this.dir + newPath);

        return this.readMetadata(newPath);
    }

    async remove(path: string): Promise<void> {
        fs.unlinkSync(this.dir + path);
    }

    async publish(path: string): Promise<Metadata> {
        const meta = this.readMetadata(path);

        fs.chmodSync(
            this.dir + path,
            meta.isDir
                ? LocalAdapter.Permissions.Dir.Public
                : LocalAdapter.Permissions.File.Public
        );

        meta.visibility = Visibility.Public;

        return meta;
    }

    async unpublish(path: string): Promise<Metadata> {
        const meta = this.readMetadata(path);

        fs.chmodSync(
            this.dir + path,
            meta.isDir
                ? LocalAdapter.Permissions.Dir.Private
                : LocalAdapter.Permissions.File.Private
        );

        meta.visibility = Visibility.Private;

        return meta;
    }

    async createDir(path: string): Promise<Metadata> {
        this.ensureDirectory(path);

        return this.readMetadata(path);
    }

    async removeDir(path: string): Promise<void> {
        const contents = this.list(path, true);

        for (let p of Object.keys(contents).reverse()) {
            if (contents[p].isDir) {
                fs.rmdirSync(this.dir + p);
            } else {
                fs.unlinkSync(this.dir + p);
            }
        }

        fs.rmdirSync(this.dir + path);
    }

    private readMetadata(path: string): Metadata {
        const fullPath = this.dir + path;

        const stat = fs.lstatSync(fullPath);

        return {
            path: path,
            isDir: stat.isDirectory(),
            timestamp: Math.round(stat.mtimeMs / 1000),
            size: stat.size,
            visibility: parseInt(substring(stat.mode.toString(8), -4), 8) & 0o044
                ? Visibility.Public
                : Visibility.Private,
        };
    }

    private static dirname(path: string) {
        const parts = path.split('/');
        parts.pop();

        return parts.join('/');
    }

    private ensureDirectory(path: string) {
        const parts = path.split('/');
        const done = [];

        for (let part of parts) {
            done.push(part);

            LocalAdapter.doEnsureDirectory(this.dir + done.join('/'))
        }
    }

    private static doEnsureDirectory(fullPath: string) {
        if (fs.existsSync(fullPath)) {
            if (!fs.lstatSync(fullPath).isDirectory()) {
                throw `Cannot create directory "${fullPath}", it's already a file`;
            }

            return;
        }

        fs.mkdirSync(fullPath);
    }
}
