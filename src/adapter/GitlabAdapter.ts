import {Metadata, Visibility, AdapterInterface} from '../types';
import * as fetch from 'node-fetch';
import { getHeaders, queryString, substring } from "../helpers";

export class GitlabAdapter implements AdapterInterface {
    private commits: { [hash: string] : number} = {};

    constructor(private projectId, private token, private branch = 'master') {
    }

    async list(directory: string, recursive: boolean): Promise<{ [path: string]: Metadata }> {
        const params = {
            path: directory,
            recursive,
        };

        const files = {};
        let page = 0;

        while (true) {
            page++;

            const hasMore = await this.request('tree?' + queryString(params) + '&page=' + page).then((res) => {
                return res.text();
            }).then(async (txt) => {
                const data = JSON.parse(txt);
                if (data.length === 0) {
                    return false;
                }

                for (let file of data) {
                    if (file.type == 'tree') {
                        files[file.path] = {
                            path: file.path,
                            isDir: true,
                            timestamp: 0,
                            size: 0,

                        }
                    } else {
                        files[file.path] = await this.getMetadata(file.path);
                    }

                    files[file.path].visibility = parseInt(substring(file.mode, -4), 8) & 0o044
                        ? Visibility.Public
                        : Visibility.Private;
                }

                return true;
            });

            if (!hasMore) {
                break;
            }
        }

        return files;
    }

    async has(path: string): Promise<boolean> {
        return await this.doesFileExist(path) || await this.doesDirectoryExist(path);
    }

    async getMetadata(path: string): Promise<Metadata> {
        return await this.request('files/' + encodeURIComponent(path) + '?ref=' + this.branch, {method: 'HEAD'}).then(await (async (res) => {
            if (res.status === 404 && await this.doesDirectoryExist(path)) {
                return {
                    path: path,
                    isDir: true,
                    timestamp: 0,
                    size: 0,
                    visibility: Visibility.Private,
                };
            }

            const headers = getHeaders(res);

            return {
                path: path,
                isDir: false,
                timestamp: await this.getCommitDate(headers['x-gitlab-last-commit-id']),
                size: parseInt(headers['x-gitlab-size']),
                visibility: Visibility.Private,
            };
        }));
    }

    async read(path: string): Promise<Buffer> {
        return await this.request('files/' + encodeURIComponent(path) + '?ref=' + this.branch).then(async res => {
            return Buffer.from(JSON.parse(await res.text()).content, 'base64');
        });
    }

    async create(path: string, content: Buffer): Promise<Metadata> {
        await this.upload('POST', path, content).then(async res => {
            if (res.status !== 201) {
                throw res.text();
            }
        });

        return await this.getMetadata(path);
    }

    async update(path: string, content: Buffer): Promise<Metadata> {
        await this.upload('PUT', path, content);

        return await this.getMetadata(path);
    }

    async move(path: string, newPath: string): Promise<Metadata> {
        await this.request('commits', {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify({
                branch: this.branch,
                commit_message: 'avris-fs update',
                actions: [
                    {
                        action: 'move',
                        file_path: newPath,
                        previous_path: path,
                    }
                ],
            }),
        });

        return await this.getMetadata(newPath);
    }

    async copy(path: string, newPath: string): Promise<Metadata> {
        await this.create(
            newPath,
            await this.read(path)
        );

        return await this.getMetadata(newPath);
    }

    async remove(path: string): Promise<void> {
        await this.request('files/' + encodeURIComponent(path), {
            method: 'DELETE',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify({
                branch: this.branch,
                commit_message: 'avris-fs update',
            }),
        });
    }

    async publish(path: string): Promise<Metadata> {
        return this.getMetadata(path);
    }

    async unpublish(path: string): Promise<Metadata> {
        return this.getMetadata(path);
    }

    async createDir(path: string): Promise<Metadata> {
        await this.create(path + '/.keep', Buffer.from(' '));

        return this.getMetadata(path);
    }

    async removeDir(path: string): Promise<void> {
        const files = await this.list(path, true);

        const actions = [];

        for (let file in files) {
            if (files.hasOwnProperty(file) && !files[file].isDir) {
                actions.push({
                    action: 'delete',
                    file_path: file,
                });
            }
        }

        await this.request('commits', {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify({
                branch: this.branch,
                commit_message: 'avris-fs update',
                actions,
            }),
        });
    }

    private async request(uri: string, options: any = {}) {
        if (options.headers === undefined) {
            options.headers = {};
        }

        options.headers['private-token'] = this.token;

        return fetch(`https://gitlab.com/api/v4/projects/${this.projectId}/repository/${uri}`, options);
    }

    private async doesFileExist(path: string): Promise<boolean> {
        return await this.request('files/' + encodeURIComponent(path) + '?ref=' + this.branch, {method: 'HEAD'}).then((res) => {
            if (res.status === 404) {
                return false;
            }

            if (res.status === 200) {
                return true;
            }

            // TODO handle access denied, >=500
        });
    }

    private async doesDirectoryExist(path: string): Promise<boolean> {
        const params = {
            path,
            per_page: 1,
        };

        return await this.request('tree?' + queryString(params)).then((res) => {
            return res.text();
        }).then((txt) => {
            return JSON.parse(txt).length > 0;
        });
    }

    private async upload(method: string, path: string, content: Buffer) {
        return this.request('files/' + encodeURIComponent(path), {
            method,
            headers: {
                'content-type': 'application/json',
            },
            body: JSON.stringify({
                branch: this.branch,
                commit_message: 'avris-fs update',
                content: content.toString(),
            }),
        });
    }

    private async getCommitDate(hash: string): Promise<number> {
        if (this.commits[hash] != undefined) {
            return this.commits[hash];
        }

        const timestamp = await this.request('commits/' + hash).then(res => {
            return res.text();
        }).then((txt) => {
            return Math.round(+new Date(JSON.parse(txt)['committed_date']) / 1000);
        });

        this.commits[hash] = timestamp;

        return timestamp;
    }
}
