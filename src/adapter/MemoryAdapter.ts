import {AdapterInterface, Metadata, BufferFile, Visibility} from "../types";
import {contains, startsWith, substring} from "../helpers";

export class MemoryAdapter implements AdapterInterface {
    private files: {[path: string]: BufferFile} = {};

    async list(directory: string, recursive: boolean): Promise<{ [path: string]: Metadata }> {
        const objects = {};

        for (let path in this.files) {
            if (path === directory) {
                continue;
            }

            if (!startsWith(path, directory)) {
                continue;
            }

            if (!recursive && contains(substring(path, directory.length + 1), '/')) {
                continue;
            }

            objects[path] = this.files[path];
        }

        return objects;
    }

    async has(path: string): Promise<boolean> {
        return this.files.hasOwnProperty(path);
    }

    async getMetadata(path: string): Promise<Metadata> {
        return this.files[path];
    }

    async read(path: string): Promise<Buffer> {
        return this.files[path].content;
    }

    async create(path: string, content: Buffer): Promise<Metadata> {
        this.ensureDirectory(MemoryAdapter.dirname(path));

        const file: BufferFile = {
            path: path,
            isDir: false,
            timestamp: Math.round(+Date.now() / 1000),
            size: content.length,
            visibility: Visibility.Private,
            content: content,
        };

        this.files[path] = file;

        return file;
    }

    async update(path: string, content: Buffer): Promise<Metadata> {
        const file = this.files[path];

        file.content = content;
        file.size = content.length;

        return file;
    }

    async move(path: string, newPath: string): Promise<Metadata> {
        this.ensureDirectory(MemoryAdapter.dirname(newPath));

        const file = this.files[path];
        file.path = newPath;
        this.files[newPath] = file;
        delete(this.files[path]);

        return file;
    }

    async copy(path: string, newPath: string): Promise<Metadata> {
        this.ensureDirectory(MemoryAdapter.dirname(newPath));

        const file = this.files[path];
        const newFile = <BufferFile> {...file, path: newPath};

        this.files[newPath] = newFile;

        return newFile;

    }

    async remove(path: string): Promise<void> {
        delete(this.files[path]);
    }

    async publish(path: string): Promise<Metadata> {
        const file = this.files[path];

        file.visibility = Visibility.Public;

        return file;
    }

    async unpublish(path: string): Promise<Metadata> {
        const file = this.files[path];

        file.visibility = Visibility.Private;

        return file;
    }

    async createDir(path: string): Promise<Metadata> {
        this.ensureDirectory(path);

        return await this.getMetadata(path);
    }

    async removeDir(path: string): Promise<void> {
        for (let file in this.files) {
            if (startsWith(file, path)) {
                delete(this.files[file]);
            }
        }
    }

    private static dirname(path: string) {
        const parts = path.split('/');
        parts.pop();

        return parts.join('/');
    }

    private ensureDirectory(path: string) {
        const parts = path.split('/');
        const done = [];

        for (let part of parts) {
            done.push(part);

            this.doEnsureDirectory(done.join('/'))
        }
    }

    private doEnsureDirectory(path: string) {
        if (this.files.hasOwnProperty(path)) {
            if (!this.files[path].isDir) {
                throw `Cannot create directory "${path}", it's already a file`;
            }

            return;
        }

        this.files[path] = <BufferFile> {
            path: path,
            isDir: true,
            timestamp: Math.round(+Date.now() / 1000),
            size: 0,
            visibility: Visibility.Private,
            content: null,
        };
    }
}
