# Avris Filesystem

A filesystem management abstraction layer for JavaScript.
Manage files on local filesystem, on FTP server, on Amazon S3, etc. all through the same interface.

**Warning!** It's a _very_ early version! 

## Example

    import {AvrisFilesystem} from "avris-fs";
    import {LocalAdapter} from "avris-fs/dist/adapter/LocalAdapter";

    const afs = new AvrisFilesystem(
        new LocalAdapter(__dirname + '/data/')
    );
    
    afs.put(                    // put = create or update
        'dir/subdir/foo.txt',
         'OSIEM!'
    ).then((meta) => {
        afs.read('dir/subdir/foo.txt').then((content) => {
            console.log(content);  // 'OSIEM!'
        });
    })

    afs.create('dir/bar.txt', 'OK!').then(() => {
        afs.list('dir', true).then((objects) => {
            console.log(objects);
        });
    });

## Available adapters

 * `LocalAdapter`
 * `MemoryAdapter`
 * `NullAdapter`
 * `S3Adapter`
 * `SftpAdapter`
 * `BrowserStorageAdapter` (`localStorage` or `sessionStorage`)
 * `GitlabAdapter`

## API

    list(directory: string = '', recursive: boolean = false): Promise<{ [path: string]: Metadata }>;
    has(path: string): Promise<boolean>;
    getMetadata(path: string): Promise<Metadata>;
    read(path: string): Promise<Buffer>;
    create(path: string, content: Buffer): Promise<Metadata>;
    update(path: string, content: Buffer): Promise<Metadata>;
    put(path: string, content: Buffer): Promise<Metadata>;
    move(path: string, newPath: string): Promise<Metadata>;
    copy(path: string, newPath: string): Promise<Metadata>;
    remove(path: string): Promise<void>;
    publish(path: string): Promise<Metadata>;
    unpublish(path: string): Promise<Metadata>;
    createDir(path: string): Promise<Metadata>;
    removeDir(path: string): Promise<void>;
    
## Credits

Greatly inspired by [Flysystem](https://flysystem.thephpleague.com/docs/), a similar project for PHP.

## Copyright

* **Author:** Andrzej Prusinowski [(Avris.it)](https://avris.it)
* **Licence:** [MIT](https://mit.avris.it)
