"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
const fs = require("fs");
const helpers_1 = require("../helpers");
class LocalAdapter {
    constructor(dir) {
        this.dir = helpers_1.rtrim(dir.replace('\\', '/'), '/') + '/';
        if (!fs.existsSync(this.dir) || !fs.lstatSync(this.dir).isDirectory()) {
            throw `Cannot use "${this.dir}" as a root. Make sure it exists and is a directory.`;
        }
    }
    list(directory, recursive) {
        return __awaiter(this, void 0, void 0, function* () {
            const objects = {};
            for (let file of fs.readdirSync(this.dir + directory)) {
                const path = helpers_1.trim(directory + '/' + file, '/');
                const object = this.readMetadata(path);
                objects[path] = object;
                if (recursive && object.isDir) {
                    const subObjects = yield this.list(path, recursive);
                    for (let subObject in subObjects) {
                        objects[subObject] = subObjects[subObject];
                    }
                }
            }
            return objects;
        });
    }
    has(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return fs.existsSync(this.dir + path);
        });
    }
    getMetadata(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.readMetadata(path);
        });
    }
    read(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return fs.readFileSync(this.dir + path);
        });
    }
    create(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(LocalAdapter.dirname(path));
            fs.writeFileSync(this.dir + path, content, { mode: LocalAdapter.Permissions.File.Private });
            return this.readMetadata(path);
        });
    }
    update(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            fs.writeFileSync(this.dir + path, content);
            return this.readMetadata(path);
        });
    }
    move(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(LocalAdapter.dirname(newPath));
            fs.renameSync(this.dir + path, this.dir + newPath);
            return this.readMetadata(newPath);
        });
    }
    copy(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(LocalAdapter.dirname(newPath));
            fs.copyFileSync(this.dir + path, this.dir + newPath);
            return this.readMetadata(newPath);
        });
    }
    remove(path) {
        return __awaiter(this, void 0, void 0, function* () {
            fs.unlinkSync(this.dir + path);
        });
    }
    publish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const meta = this.readMetadata(path);
            fs.chmodSync(this.dir + path, meta.isDir
                ? LocalAdapter.Permissions.Dir.Public
                : LocalAdapter.Permissions.File.Public);
            meta.visibility = types_1.Visibility.Public;
            return meta;
        });
    }
    unpublish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const meta = this.readMetadata(path);
            fs.chmodSync(this.dir + path, meta.isDir
                ? LocalAdapter.Permissions.Dir.Private
                : LocalAdapter.Permissions.File.Private);
            meta.visibility = types_1.Visibility.Private;
            return meta;
        });
    }
    createDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(path);
            return this.readMetadata(path);
        });
    }
    removeDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const contents = this.list(path, true);
            for (let p of Object.keys(contents).reverse()) {
                if (contents[p].isDir) {
                    fs.rmdirSync(this.dir + p);
                }
                else {
                    fs.unlinkSync(this.dir + p);
                }
            }
            fs.rmdirSync(this.dir + path);
        });
    }
    readMetadata(path) {
        const fullPath = this.dir + path;
        const stat = fs.lstatSync(fullPath);
        return {
            path: path,
            isDir: stat.isDirectory(),
            timestamp: Math.round(stat.mtimeMs / 1000),
            size: stat.size,
            visibility: parseInt(helpers_1.substring(stat.mode.toString(8), -4), 8) & 0o044
                ? types_1.Visibility.Public
                : types_1.Visibility.Private,
        };
    }
    static dirname(path) {
        const parts = path.split('/');
        parts.pop();
        return parts.join('/');
    }
    ensureDirectory(path) {
        const parts = path.split('/');
        const done = [];
        for (let part of parts) {
            done.push(part);
            LocalAdapter.doEnsureDirectory(this.dir + done.join('/'));
        }
    }
    static doEnsureDirectory(fullPath) {
        if (fs.existsSync(fullPath)) {
            if (!fs.lstatSync(fullPath).isDirectory()) {
                throw `Cannot create directory "${fullPath}", it's already a file`;
            }
            return;
        }
        fs.mkdirSync(fullPath);
    }
}
LocalAdapter.Permissions = {
    File: {
        Public: 0o644,
        Private: 0o600,
    },
    Dir: {
        Public: 0o755,
        Private: 0o700,
    }
};
exports.LocalAdapter = LocalAdapter;
