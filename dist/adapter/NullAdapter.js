"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
class NullAdapter {
    list(directory, recursive) {
        return __awaiter(this, void 0, void 0, function* () {
            return {};
        });
    }
    has(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return true;
        });
    }
    getMetadata(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return {
                path: path,
                isDir: false,
                timestamp: Math.round(+Date.now() / 1000),
                size: 0,
                visibility: types_1.Visibility.Private,
            };
        });
    }
    read(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return Buffer.from('');
        });
    }
    create(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.getMetadata(path);
        });
    }
    update(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.getMetadata(path);
        });
    }
    move(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.getMetadata(newPath);
        });
    }
    copy(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.getMetadata(newPath);
        });
    }
    remove(path) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
    publish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.getMetadata(path);
        });
    }
    unpublish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.getMetadata(path);
        });
    }
    createDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.getMetadata(path);
        });
    }
    removeDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
        });
    }
}
exports.NullAdapter = NullAdapter;
