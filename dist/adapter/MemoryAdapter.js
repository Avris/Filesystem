"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
const helpers_1 = require("../helpers");
class MemoryAdapter {
    constructor() {
        this.files = {};
    }
    list(directory, recursive) {
        return __awaiter(this, void 0, void 0, function* () {
            const objects = {};
            for (let path in this.files) {
                if (path === directory) {
                    continue;
                }
                if (!helpers_1.startsWith(path, directory)) {
                    continue;
                }
                if (!recursive && helpers_1.contains(helpers_1.substring(path, directory.length + 1), '/')) {
                    continue;
                }
                objects[path] = this.files[path];
            }
            return objects;
        });
    }
    has(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.files.hasOwnProperty(path);
        });
    }
    getMetadata(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.files[path];
        });
    }
    read(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.files[path].content;
        });
    }
    create(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(MemoryAdapter.dirname(path));
            const file = {
                path: path,
                isDir: false,
                timestamp: Math.round(+Date.now() / 1000),
                size: content.length,
                visibility: types_1.Visibility.Private,
                content: content,
            };
            this.files[path] = file;
            return file;
        });
    }
    update(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            const file = this.files[path];
            file.content = content;
            file.size = content.length;
            return file;
        });
    }
    move(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(MemoryAdapter.dirname(newPath));
            const file = this.files[path];
            file.path = newPath;
            this.files[newPath] = file;
            delete (this.files[path]);
            return file;
        });
    }
    copy(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(MemoryAdapter.dirname(newPath));
            const file = this.files[path];
            const newFile = Object.assign({}, file, { path: newPath });
            this.files[newPath] = newFile;
            return newFile;
        });
    }
    remove(path) {
        return __awaiter(this, void 0, void 0, function* () {
            delete (this.files[path]);
        });
    }
    publish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const file = this.files[path];
            file.visibility = types_1.Visibility.Public;
            return file;
        });
    }
    unpublish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const file = this.files[path];
            file.visibility = types_1.Visibility.Private;
            return file;
        });
    }
    createDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(path);
            return yield this.getMetadata(path);
        });
    }
    removeDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            for (let file in this.files) {
                if (helpers_1.startsWith(file, path)) {
                    delete (this.files[file]);
                }
            }
        });
    }
    static dirname(path) {
        const parts = path.split('/');
        parts.pop();
        return parts.join('/');
    }
    ensureDirectory(path) {
        const parts = path.split('/');
        const done = [];
        for (let part of parts) {
            done.push(part);
            this.doEnsureDirectory(done.join('/'));
        }
    }
    doEnsureDirectory(path) {
        if (this.files.hasOwnProperty(path)) {
            if (!this.files[path].isDir) {
                throw `Cannot create directory "${path}", it's already a file`;
            }
            return;
        }
        this.files[path] = {
            path: path,
            isDir: true,
            timestamp: Math.round(+Date.now() / 1000),
            size: 0,
            visibility: types_1.Visibility.Private,
            content: null,
        };
    }
}
exports.MemoryAdapter = MemoryAdapter;
