"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
const fetch = require("node-fetch");
const xml_js_1 = require("xml-js");
const helpers_1 = require("../helpers");
class S3Adapter {
    constructor(bucket, key, secret) {
        this.bucket = bucket;
        this.key = key;
        this.secret = secret;
        this.PUBLIC_GRANT_URL = 'http://acs.amazonaws.com/groups/global/AllUsers';
    }
    list(directory, recursive) {
        return __awaiter(this, void 0, void 0, function* () {
            const params = {
                'list-type': '2',
                'prefix': helpers_1.ltrim(helpers_1.rtrim(directory, '/') + '/', '/'),
                'delimiter': recursive ? '' : '/',
            };
            return yield this.request('?' + helpers_1.queryString(params)).then((res) => {
                return res.text();
            }).then((txt) => {
                const data = xml_js_1.xml2js(txt, { compact: true })['ListBucketResult'];
                // TODO data.IsTruncated pagination
                const objects = {};
                for (let obj of helpers_1.arrayify(data.Contents)) {
                    const path = obj.Key._text;
                    const isDir = helpers_1.endsWith(path, '/');
                    const ts = +new Date(obj.LastModified._text) / 1000;
                    objects[path] = {
                        path: path,
                        isDir: isDir,
                        timestamp: ts,
                        size: parseInt(obj.Size._text),
                        visibility: null,
                    };
                    if (!isDir) {
                        const parts = path.split('/');
                        for (let i = 1; i < parts.length; i++) {
                            const key = parts.slice(0, i).join('/') + '/';
                            if (objects[key] === undefined) {
                                objects[key] = {
                                    path: key,
                                    isDir: true,
                                    timestamp: 0,
                                    size: 0,
                                    visibility: null,
                                };
                            }
                        }
                    }
                }
                for (let obj of helpers_1.arrayify(data.CommonPrefixes)) {
                    const path = obj.Prefix._text;
                    objects[path] = {
                        path: path,
                        isDir: true,
                        timestamp: 0,
                        size: 0,
                        visibility: null,
                    };
                }
                return objects;
            });
        });
    }
    has(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield this.doesFileExist(path)) || (yield this.doesDirectoryExist(path));
        });
    }
    getMetadata(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.request(encodeURIComponent(path), { method: 'HEAD' }).then(yield ((res) => __awaiter(this, void 0, void 0, function* () {
                if (res.status === 404 && (yield this.doesDirectoryExist(path))) {
                    return {
                        path: path,
                        isDir: true,
                        timestamp: 0,
                        size: 0,
                        visibility: types_1.Visibility.Private,
                    };
                }
                const headers = helpers_1.getHeaders(res);
                return {
                    path: path,
                    isDir: false,
                    timestamp: Math.round(+new Date(headers['last-modified']) / 1000),
                    size: parseInt(headers['content-length']),
                    visibility: yield this.getVisibility(path),
                };
            })));
        });
    }
    read(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.request(encodeURIComponent(path)).then((res) => {
                return res.arrayBuffer();
            }).then((ab) => {
                return helpers_1.toBuffer(ab);
            });
        });
    }
    create(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.upload(path, content);
            return yield this.getMetadata(path);
        });
    }
    update(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.upload(path, content);
            return yield this.getMetadata(path);
        });
    }
    move(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const meta = yield this.copy(path, newPath);
            yield this.remove(path);
            return meta;
        });
    }
    copy(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.request(encodeURIComponent(newPath), {
                method: 'PUT',
                headers: {
                    'x-amz-copy-source': `/${this.bucket}/${encodeURIComponent(path)}`,
                }
            });
            return yield this.getMetadata(newPath);
        });
    }
    remove(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.request(encodeURIComponent(path), { method: 'DELETE' });
        });
    }
    publish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.request(encodeURIComponent(path), {
                method: 'PUT',
                action: 'acl',
                headers: {
                    'x-amz-acl': 'public-read',
                }
            });
            return this.getMetadata(path);
        });
    }
    unpublish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.request(encodeURIComponent(path), {
                method: 'PUT',
                action: 'acl',
                headers: {
                    'x-amz-acl': 'private',
                }
            });
            return this.getMetadata(path);
        });
    }
    createDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.upload(path + '/', Buffer.from(''));
            return this.getMetadata(path);
        });
    }
    removeDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const files = yield this.list(path, true);
            const removePromises = [];
            for (let file in files) {
                if (files.hasOwnProperty(file) && !files[file].isDir) {
                    removePromises.push(this.remove(file));
                }
            }
            if (removePromises.length) {
                yield Promise.all(removePromises);
            }
        });
    }
    request(uri, options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const date = new Date().toUTCString();
            if (options.headers === undefined) {
                options.headers = {};
            }
            options.headers['date'] = date;
            options.headers['authorization'] = this.buildAuthHeader(uri, options);
            if (options.action !== undefined) {
                uri += '?' + options.action;
                delete options.action;
            }
            return fetch(`https://${this.bucket}.s3.amazonaws.com/${uri}`, options);
        });
    }
    buildAuthHeader(uri, options) {
        let stringToSign = '';
        stringToSign += options.method || 'GET';
        stringToSign += "\n";
        // TODO content
        stringToSign += "\n";
        stringToSign += options.headers['content-type'] || '';
        stringToSign += "\n";
        stringToSign += options.headers['date'];
        stringToSign += "\n";
        for (let key in options.headers) {
            if (options.headers.hasOwnProperty(key) && helpers_1.startsWith(key, 'x-amz-')) {
                stringToSign += key + ':' + options.headers[key];
                stringToSign += "\n";
            }
        }
        const qmPos = uri.indexOf('?');
        if (qmPos >= 0) {
            uri = uri.slice(0, qmPos);
        }
        if (options.action !== undefined) {
            uri += '?' + options.action;
        }
        stringToSign += `/${this.bucket}/${uri}`;
        const signature = helpers_1.hmacSha1(stringToSign, this.secret);
        return `AWS ${this.key}:${signature}`;
    }
    doesFileExist(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.request(encodeURIComponent(path), { method: 'HEAD' }).then((res) => {
                if (res.status === 404) {
                    return false;
                }
                if (res.status === 200) {
                    return true;
                }
                // TODO handle access denied, >=500
            });
        });
    }
    doesDirectoryExist(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const params = {
                'list-type': '2',
                'prefix': helpers_1.ltrim(helpers_1.rtrim(path, '/') + '/', '/'),
                'max-keys': '1',
            };
            return yield this.request('?' + helpers_1.queryString(params)).then((res) => {
                return res.text();
            }).then((txt) => {
                const data = xml_js_1.xml2js(txt, { compact: true })['ListBucketResult'];
                return helpers_1.arrayify(data.Contents).length + helpers_1.arrayify(data.CommonPrefixes).length > 0;
            });
        });
    }
    getVisibility(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.request(encodeURIComponent(path), { action: 'acl' }).then((res) => {
                return res.text();
            }).then((txt) => {
                const data = xml_js_1.xml2js(txt, { compact: true })['AccessControlPolicy'];
                for (let grant of helpers_1.arrayify(data['AccessControlList']['Grant'])) {
                    if (grant['Grantee']['URI']
                        && grant['Grantee']['URI']._text === this.PUBLIC_GRANT_URL
                        && grant['Permission']._text === 'READ') {
                        return types_1.Visibility.Public;
                    }
                }
                return types_1.Visibility.Private;
            });
        });
    }
    upload(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.request(encodeURIComponent(path), {
                method: 'PUT',
                body: content,
                headers: {
                    'content-type': 'text/plaintext',
                    'content-length': content.length,
                }
            });
        });
    }
}
exports.S3Adapter = S3Adapter;
