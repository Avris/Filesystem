"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
const helpers_1 = require("../helpers");
class BrowserStorageAdapter {
    constructor(storage, prefix = 'afs-') {
        this.prefix = prefix;
        this.storage = storage || window.localStorage;
    }
    list(directory, recursive) {
        return __awaiter(this, void 0, void 0, function* () {
            const objects = {};
            for (let path in this.storage) {
                if (!helpers_1.startsWith(path, this.prefix)) {
                    continue;
                }
                path = path.substring(this.prefix.length);
                if (path === directory) {
                    continue;
                }
                if (!helpers_1.startsWith(path, directory)) {
                    continue;
                }
                if (!recursive && helpers_1.contains(helpers_1.substring(path, directory.length + 1), '/')) {
                    continue;
                }
                objects[path] = JSON.parse(this.storage.getItem(this.prefix + path));
            }
            return objects;
        });
    }
    has(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.storage.getItem(this.prefix + path) !== null;
        });
    }
    getMetadata(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return JSON.parse(this.storage.getItem(this.prefix + path));
        });
    }
    read(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return Buffer.from(JSON.parse(this.storage.getItem(this.prefix + path)).content, 'base64');
        });
    }
    create(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(BrowserStorageAdapter.dirname(path));
            const file = {
                path: path,
                isDir: false,
                timestamp: Math.round(+Date.now() / 1000),
                size: content.length,
                visibility: types_1.Visibility.Private,
                content: content.toString('base64'),
            };
            this.storage.setItem(this.prefix + path, JSON.stringify(file));
            return file;
        });
    }
    update(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            const file = JSON.parse(this.storage.getItem(this.prefix + path));
            file.content = content.toString('base64');
            file.size = content.length;
            this.storage.setItem(this.prefix + path, JSON.stringify(file));
            return file;
        });
    }
    move(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(BrowserStorageAdapter.dirname(newPath));
            const file = JSON.parse(this.storage.getItem(this.prefix + path));
            file.path = newPath;
            this.storage.setItem(this.prefix + newPath, JSON.stringify(file));
            this.storage.removeItem(this.prefix + path);
            return file;
        });
    }
    copy(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(BrowserStorageAdapter.dirname(newPath));
            const file = JSON.parse(this.storage.getItem(this.prefix + path));
            const newFile = Object.assign({}, file, { path: newPath });
            this.storage.setItem(this.prefix + newPath, JSON.stringify(file));
            return newFile;
        });
    }
    remove(path) {
        return __awaiter(this, void 0, void 0, function* () {
            this.storage.removeItem(this.prefix + path);
        });
    }
    publish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const file = JSON.parse(this.storage.getItem(this.prefix + path));
            file.visibility = types_1.Visibility.Public;
            this.storage.setItem(this.prefix + path, JSON.stringify(file));
            return file;
        });
    }
    unpublish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const file = JSON.parse(this.storage.getItem(this.prefix + path));
            file.visibility = types_1.Visibility.Private;
            this.storage.setItem(this.prefix + path, JSON.stringify(file));
            return file;
        });
    }
    createDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            this.ensureDirectory(path);
            return yield this.getMetadata(path);
        });
    }
    removeDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            for (let file in this.storage) {
                if (helpers_1.startsWith(file, this.prefix + path)) {
                    this.storage.removeItem(this.prefix + file);
                }
            }
        });
    }
    static dirname(path) {
        const parts = path.split('/');
        parts.pop();
        return parts.join('/');
    }
    ensureDirectory(path) {
        const parts = path.split('/');
        const done = [];
        for (let part of parts) {
            done.push(part);
            this.doEnsureDirectory(done.join('/'));
        }
    }
    doEnsureDirectory(path) {
        if (this.storage.getItem(this.prefix + path) !== null) {
            if (!JSON.parse(this.storage.getItem(this.prefix + path)).isDir) {
                throw `Cannot create directory "${path}", it's already a file`;
            }
            return;
        }
        const file = {
            path: path,
            isDir: true,
            timestamp: Math.round(+Date.now() / 1000),
            size: 0,
            visibility: types_1.Visibility.Private,
            content: null,
        };
        this.storage.setItem(this.prefix + path, JSON.stringify(file));
    }
}
exports.BrowserStorageAdapter = BrowserStorageAdapter;
