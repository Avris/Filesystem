"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const types_1 = require("../types");
const fs = require("fs");
const FTPClient = require("ssh2-sftp-client");
const helpers_1 = require("../helpers");
class SftpAdapter {
    constructor(host, user, key, dir) {
        this.host = host;
        this.user = user;
        this.key = key;
        this.dir = dir;
        this.client = null;
        this.dir = helpers_1.rtrim(dir.replace('\\', '/'), '/') + '/';
        // TODO this.client.end();
    }
    list(directory, recursive) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            const objects = {};
            const fetchList = (dir) => __awaiter(this, void 0, void 0, function* () {
                yield this.client.list(this.dir + dir).then(yield ((res) => __awaiter(this, void 0, void 0, function* () {
                    for (let obj of res) {
                        const path = dir + '/' + obj.name;
                        if (obj.type === 'd') {
                            objects[path] = {
                                path: path,
                                isDir: true,
                                timestamp: Math.round(obj.modifyTime / 1000),
                                size: obj.size,
                                visibility: SftpAdapter.getVisibility(obj),
                            };
                            if (recursive) {
                                yield fetchList(dir + '/' + obj.name);
                            }
                        }
                        else {
                            objects[path] = {
                                path: path,
                                isDir: false,
                                timestamp: Math.round(obj.modifyTime / 1000),
                                size: obj.size,
                                visibility: SftpAdapter.getVisibility(obj),
                            };
                        }
                    }
                })));
            });
            yield fetchList(directory);
            return objects;
        });
    }
    has(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            const [dir, name] = SftpAdapter.splitIntoDirName(path);
            return yield this.client.list(this.dir + dir).then(yield ((res) => __awaiter(this, void 0, void 0, function* () {
                for (let obj of res) {
                    if (obj.name === name) {
                        return true;
                    }
                }
                return false;
            }))).catch((err) => {
                return false;
            });
        });
    }
    getMetadata(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            const [dir, name] = SftpAdapter.splitIntoDirName(path);
            return yield this.client.list(this.dir + dir).then(yield ((res) => __awaiter(this, void 0, void 0, function* () {
                for (let obj of res) {
                    if (obj.name === name) {
                        return {
                            path: path,
                            isDir: obj.type === 'd',
                            timestamp: Math.round(obj.modifyTime / 1000),
                            size: obj.size,
                            visibility: SftpAdapter.getVisibility(obj),
                        };
                    }
                }
            })));
        });
    }
    read(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            return (yield this.client.get(this.dir + path, true, null));
        });
    }
    create(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            yield this.ensureDirectory(path);
            yield this.client.put(content, this.dir + path);
            return yield this.getMetadata(path);
        });
    }
    update(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            yield this.ensureDirectory(path);
            yield this.client.put(content, this.dir + path);
            return yield this.getMetadata(path);
        });
    }
    move(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            yield this.ensureDirectory(newPath);
            yield this.client.rename(this.dir + path, this.dir + newPath);
            return yield this.getMetadata(newPath);
        });
    }
    copy(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            yield this.ensureDirectory(newPath);
            yield this.client.get(this.dir + path, true, null).then(yield ((stream) => __awaiter(this, void 0, void 0, function* () {
                yield this.client.put(stream, this.dir + newPath, true, null);
            })));
            return yield this.getMetadata(newPath);
        });
    }
    remove(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            yield this.client.delete(this.dir + path);
        });
    }
    publish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            const meta = yield this.getMetadata(path);
            yield this.client.chmod(this.dir + path, meta.isDir
                ? SftpAdapter.Permissions.Dir.Public
                : SftpAdapter.Permissions.File.Public);
            meta.visibility = types_1.Visibility.Public;
            return meta;
        });
    }
    unpublish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            const meta = yield this.getMetadata(path);
            yield this.client.chmod(this.dir + path, meta.isDir
                ? SftpAdapter.Permissions.Dir.Private
                : SftpAdapter.Permissions.File.Private);
            meta.visibility = types_1.Visibility.Private;
            return meta;
        });
    }
    createDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            yield this.client.mkdir(this.dir + path, true);
            return yield this.getMetadata(path);
        });
    }
    removeDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.connect();
            yield this.client.rmdir(this.dir + path, true);
        });
    }
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.client === null) {
                this.client = new FTPClient();
                yield this.client.connect({
                    host: this.host,
                    username: this.user,
                    privateKey: fs.readFileSync(this.key),
                });
            }
            return this.client;
        });
    }
    static getVisibility(obj) {
        return obj.rights.other.indexOf('r') > -1
            ? types_1.Visibility.Public
            : types_1.Visibility.Private;
    }
    static splitIntoDirName(path) {
        const parts = path.split('/');
        const name = parts.pop();
        const dir = parts.join('/');
        return [dir, name];
    }
    ensureDirectory(path) {
        return __awaiter(this, void 0, void 0, function* () {
            const [dir, name] = SftpAdapter.splitIntoDirName(path);
            if (!(yield this.has(dir))) {
                yield this.client.mkdir(this.dir + dir, true);
            }
        });
    }
}
SftpAdapter.Permissions = {
    File: {
        Public: 0o644,
        Private: 0o600,
    },
    Dir: {
        Public: 0o755,
        Private: 0o700,
    }
};
exports.SftpAdapter = SftpAdapter;
