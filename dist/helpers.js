"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util_1 = require("util");
const jssha = require("jssha");
function trim(str, chr = '\\s') {
    return str.replace(new RegExp('^' + chr + '+|' + chr + '+$', 'g'), '');
}
exports.trim = trim;
function ltrim(str, chr = '\\s') {
    return str.replace(new RegExp('^' + chr + '+'), '');
}
exports.ltrim = ltrim;
function rtrim(str, chr = '\\s') {
    return str.replace(new RegExp(chr + '+$'), '');
}
exports.rtrim = rtrim;
function substring(str, start, end) {
    if (start < 0) {
        start = str.length + start;
    }
    if (end && end < 0) {
        end = str.length + end;
    }
    return str.substring(start, end);
}
exports.substring = substring;
function startsWith(haystack, needle) {
    return substring(haystack, 0, needle.length) === needle;
}
exports.startsWith = startsWith;
function endsWith(haystack, needle) {
    return substring(haystack, haystack.length - needle.length) === needle;
}
exports.endsWith = endsWith;
function contains(haystack, needle) {
    return haystack.indexOf(needle) >= 0;
}
exports.contains = contains;
function dump(...objects) {
    console.log(...objects.map((obj) => util_1.inspect(obj, { colors: true, depth: Infinity })));
}
exports.dump = dump;
function hmacSha1(message, key) {
    const shaObj = new jssha('SHA-1', 'TEXT');
    shaObj.setHMACKey(key, 'TEXT');
    shaObj.update(message);
    return shaObj.getHMAC('B64');
}
exports.hmacSha1 = hmacSha1;
function queryString(parameters) {
    const encoded = [];
    for (let key in parameters) {
        if (parameters.hasOwnProperty(key)) {
            encoded.push(encodeURIComponent(key) + '=' + encodeURIComponent(parameters[key]));
        }
    }
    return encoded.join('&');
}
exports.queryString = queryString;
function arrayify(obj) {
    if (obj === undefined) {
        return [];
    }
    if (Array.isArray(obj)) {
        return obj;
    }
    return [obj];
}
exports.arrayify = arrayify;
function getHeaders(res) {
    const headers = {};
    res.headers.forEach(function (value, name) {
        headers[name] = value;
    });
    return headers;
}
exports.getHeaders = getHeaders;
function toArrayBuffer(buffer) {
    const ab = new ArrayBuffer(buffer.length);
    const view = new Uint8Array(ab);
    for (let i = 0; i < buffer.length; ++i) {
        view[i] = buffer[i];
    }
    return ab;
}
exports.toArrayBuffer = toArrayBuffer;
function toBuffer(ab) {
    const buf = Buffer.alloc(ab.byteLength);
    const view = new Uint8Array(ab);
    for (let i = 0; i < buf.length; ++i) {
        buf[i] = view[i];
    }
    return buf;
}
exports.toBuffer = toBuffer;
