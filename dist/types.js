"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Visibility;
(function (Visibility) {
    Visibility["Private"] = "private";
    Visibility["Public"] = "public";
})(Visibility = exports.Visibility || (exports.Visibility = {}));
class Metadata {
}
exports.Metadata = Metadata;
class BufferFile extends Metadata {
}
exports.BufferFile = BufferFile;
class StringFile extends Metadata {
}
exports.StringFile = StringFile;
