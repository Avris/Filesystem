"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class AvrisFilesystem {
    constructor(adapter) {
        this.adapter = adapter;
    }
    list(directory = '', recursive = false) {
        return __awaiter(this, void 0, void 0, function* () {
            return this.adapter.list(AvrisFilesystem.normalisePath(directory), recursive);
        });
    }
    has(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.has(AvrisFilesystem.normalisePath(path));
        });
    }
    getMetadata(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.getMetadata(yield this.assertPresent(AvrisFilesystem.normalisePath(path)));
        });
    }
    read(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.read(yield this.assertPresent(AvrisFilesystem.normalisePath(path)));
        });
    }
    create(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.create(yield this.assertAbsent(AvrisFilesystem.normalisePath(path)), content);
        });
    }
    update(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.update(yield this.assertPresent(AvrisFilesystem.normalisePath(path)), content);
        });
    }
    put(path, content) {
        return __awaiter(this, void 0, void 0, function* () {
            path = AvrisFilesystem.normalisePath(path);
            if (yield this.adapter.has(path)) {
                return yield this.adapter.update(path, content);
            }
            return yield this.adapter.create(path, content);
        });
    }
    move(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.move(yield this.assertPresent(AvrisFilesystem.normalisePath(path)), yield this.assertAbsent(AvrisFilesystem.normalisePath(newPath)));
        });
    }
    copy(path, newPath) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.copy(yield this.assertPresent(AvrisFilesystem.normalisePath(path)), yield this.assertAbsent(AvrisFilesystem.normalisePath(newPath)));
        });
    }
    remove(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.adapter.remove(yield this.assertPresent(AvrisFilesystem.normalisePath(path)));
        });
    }
    publish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.publish(yield this.assertPresent(AvrisFilesystem.normalisePath(path)));
        });
    }
    unpublish(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.unpublish(yield this.assertPresent(AvrisFilesystem.normalisePath(path)));
        });
    }
    createDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this.adapter.createDir(yield this.assertAbsent(AvrisFilesystem.normalisePath(path)));
        });
    }
    removeDir(path) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.adapter.removeDir(yield this.assertPresent(AvrisFilesystem.normalisePath(path)));
        });
    }
    assertPresent(path) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!(yield this.adapter.has(path))) {
                throw `File "${path}" does not exist`;
            }
            return path;
        });
    }
    assertAbsent(path) {
        return __awaiter(this, void 0, void 0, function* () {
            if (yield this.adapter.has(path)) {
                throw `File "${path}" already exists`;
            }
            return path;
        });
    }
    static normalisePath(path) {
        const parts = [];
        for (let part of path.replace('\\', '/').split('/')) {
            switch (part) {
                case '':
                case '.':
                    break;
                case '..':
                    if (parts.length === 0) {
                        throw `Path "${path}" outside of the root`;
                    }
                    parts.pop();
                    break;
                default:
                    parts.push(part);
            }
        }
        return parts.join('/');
    }
}
exports.AvrisFilesystem = AvrisFilesystem;
